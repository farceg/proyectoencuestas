@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form class='post' action=''>AL CREAR, REDIRECCIONAR A AGREGARPREGUNTAS Y 
                        ENVIAR EL ID QUE SE ACABA DE CREAR PARA ESTA ENCUESTA.
                        <div class="form-group">
                            <label for="descripcion_encuesta">Nombre de la encuesta</label>
                            <input type="text" class="form-control" id="descripcion_encuesta">
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha de inicio</label>
                            <input id="fecha_inicio" type="date" class="form-control" name="fecha_inicio">
                        </div>
                        <div class="form-group">
                            <label for="fecha_fin">Fecha de fin</label>
                            <input id="fecha_fin" type="date" class="form-control" name="fecha_fin">
                        </div>
                        <div class="form-group">
                            <label for="maximo_encuestados">Maximo a encuestar</label>
                            <input id="maximo_encuestados" type="number" class="form-control" name="maximo_encuestados">
                        </div>
                        <div class="form-group">
                            <label for="objetivo">Objetivo de la encuesta</label>
                            <textarea class="form-control" id="objetivo" rows="2"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="edad_min_objetivo">Edad minima</label>
                            <input id="edad_min_objetivo" type="number" class="form-control" name="edad_min_objetivo">
                        </div>
                        <div class="form-group">
                            <label for="edad_max_objetivo">Edad maxima</label>
                            <input id="edad_max_objetivo" type="number" class="form-control" name="edad_max_objetivo">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="es_publica">
                            <label class="form-check-label" for="es_publica">La encuesta es publica</label>
                            <h1></h1>
                        </div>
                        <div class="form-group">
                            <label for="genero_objetivo_id">Genero objetivo</label>
                            <select class="form-control" id="genero_objetivo_id">
                                <option>Ambos</option>
                                <option value="Masculino">Hombres</option>
                                <option value="Femenino">Mujeres</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Crear</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection