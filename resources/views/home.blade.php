@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Informacion de tu cuenta</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <p class="h6">Estas logeado con el correo: <a href="" class="badge badge-primary">{{ Auth::user()->email}}</a></p>
                    <p class="h6">Encuestas completadas: <a href="" class="badge badge-primary">2</a></p>
                    <p class="h6">Encuestas por terminar: <a href="" class="badge badge-primary">1</a></p>
                    <p class="h6">Encuestas creadas: <a href="" class="badge badge-primary">5</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection